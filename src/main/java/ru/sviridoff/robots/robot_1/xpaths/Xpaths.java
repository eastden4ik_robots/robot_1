package ru.sviridoff.robots.robot_1.xpaths;

public class Xpaths {

    public static final String LoginInput = "//input[contains(@name,'login')]";
    public static final String PasswordInput = "//input[contains(@name,'password')]";
    public static final String LoginButton = "//input[contains(@name,'log_in')]";

}
