package ru.sviridoff.robots.robot_1.utils;

import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;

public class Stash {



    public final static Logger.LogLevel SUCCESS = Logger.LogLevel.SUCCESS;
    public final static Logger.LogLevel INFO = Logger.LogLevel.INFO;
    public final static Logger.LogLevel WARNING = Logger.LogLevel.WARNING;
    public final static Logger.LogLevel DANGER = Logger.LogLevel.DANGER;
    public final static Logger.LogLevel STEPS = Logger.LogLevel.STEPS;

    public static Props properties;

    public static String LogFile;

}
