package ru.sviridoff.robots.robot_1.libsteps;

import ru.sviridoff.framework.logger.Logger;

import ru.sviridoff.robots.robot_1.utils.Stash;
import ru.sviridoff.robots.robot_1.utils.Utils;

import java.io.IOException;

public class ApiValidity {


    private static Utils utils = new Utils();
    private static Logger logger = new Logger();


    public void CheckApiSteps() throws IOException {
        String GET_RESPONSE = utils.sendGET(Stash.properties.getProperty("site.url.api")).get(1);
        if (GET_RESPONSE != null && GET_RESPONSE.equals("API is working...")) {
            logger.Logging(Stash.SUCCESS, GET_RESPONSE);
        } else {
            logger.Logging(Stash.DANGER, "GET Request to [" + Stash.properties.getProperty("site.url.api") + "] failed.");
        }
    }


}
