package ru.sviridoff.robots.robot_1.libsteps;

import ru.sviridoff.robots.robot_1.Models.Book;

import java.io.IOException;
import java.util.List;

public class ApiSteps {


    private static ApiValidity apiValidity = new ApiValidity();
    private static BooksApi getBooksApi = new BooksApi();


    public void MainSteps() {
        try {
            apiValidity.CheckApiSteps();
            String res = getBooksApi.GetAllJsonBooks();
            List<Book> books = getBooksApi.ParseJsonResponseBooks(res);

//            getBooksApi.PrintList(books);
            int i = 1;
            for (Book book: books) {
                getBooksApi.CheckValidityOfLink(i, book.getBOOK_LINK(), book.getBOOK_NAME());

                i++;
            }


        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


}
