package ru.sviridoff.robots.robot_1.libsteps;

import com.google.gson.Gson;
import ru.sviridoff.framework.logger.Logger;

import ru.sviridoff.robots.robot_1.Models.Book;
import ru.sviridoff.robots.robot_1.utils.Stash;
import ru.sviridoff.robots.robot_1.utils.Utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class BooksApi {

    private static Utils utils = new Utils();
    private static Gson gson = new Gson();
    private static Logger logger = new Logger();

    public String GetAllJsonBooks() throws IOException {
        String Result;
        String GET_RESPONSE = utils.sendGET(Stash.properties.getProperty("site.url.api.books")).get(1);
        if (GET_RESPONSE != null) {
            logger.Logging(Stash.SUCCESS, "JSON: " + GET_RESPONSE);
            Result = GET_RESPONSE;
        } else {
            logger.Logging(Stash.DANGER, "GET Request to [" + Stash.properties.getProperty("site.url.api.books") + "] failed.");
            Result = null;
        }
        return Result;
    }

    public String GetOneJsonBooks(String BOOK_ID) throws IOException {
        String Result;
        String GET_RESPONSE = utils.sendGET(Stash.properties.getProperty("site.url.api.book") + BOOK_ID).get(1);
        if (GET_RESPONSE != null && !GET_RESPONSE.equals("[]")) {
            logger.Logging(Stash.SUCCESS, "JSON: " + GET_RESPONSE);
            Result = GET_RESPONSE;
        } else {
            logger.Logging(Stash.DANGER, "GET Request to [" + Stash.properties.getProperty("site.url.api.book") + BOOK_ID + "] failed.");
            Result = null;
        }
        return Result;
    }

    public void CheckValidityOfLink(int ID, String URI, String bn) throws IOException {
        List<String> response = utils.sendGET(URI);
        if (response.get(0).equals("200")) {
            logger.Logging(Stash.SUCCESS, ID + " " + URI, Stash.LogFile);
        } else {
            logger.Logging(Stash.WARNING, ID + " " + response.get(0) + " :: " + URI + " :: " + bn, Stash.LogFile);
        }
    }

    public List<Book> ParseJsonResponseBooks(String response) {
        return Arrays.asList(gson.fromJson(response, Book[].class));
    }

    public Book ParseJsonResponseBook(String response) {
        return gson.fromJson(response, Book.class);
    }

    public void PrintBook(Book book) {
        logger.Logging(Stash.STEPS, book.getBOOK_NAME() + " " + book.getBOOK_LINK());
    }

    public void PrintList(List<Book> books) {
        for (Book book: books) {
            logger.Logging(Stash.STEPS, book.getBOOK_NAME() + " " + book.getBOOK_LINK());
        }
    }


}
