package ru.sviridoff.robots.robot_1.Models;

import java.util.List;

public class Book {


    private String BOOK_ID;
    private String BOOK_NAME;
    private String BOOK_IMG;
    private String BOOK_LINK;
    private String BOOK_YEAR;
    private List<String> BOOK_AUTHORS;
    private List<String> BOOK_GENRES;

    public List<String> getBOOK_AUTHORS() {
        return BOOK_AUTHORS;
    }

    public void setBOOK_AUTHORS(List<String> BOOK_AUTHORS) {
        this.BOOK_AUTHORS = BOOK_AUTHORS;
    }

    public List<String> getBOOK_GENRES() {
        return BOOK_GENRES;
    }

    public void setBOOK_GENRES(List<String> BOOK_GENRES) {
        this.BOOK_GENRES = BOOK_GENRES;
    }

    public String getBOOK_ID() {
        return BOOK_ID;
    }

    public void setBOOK_ID(String BOOK_ID) {
        this.BOOK_ID = BOOK_ID;
    }

    public String getBOOK_NAME() {
        return BOOK_NAME;
    }

    public void setBOOK_NAME(String BOOK_NAME) {
        this.BOOK_NAME = BOOK_NAME;
    }

    public String getBOOK_IMG() {
        return BOOK_IMG;
    }

    public void setBOOK_IMG(String BOOK_IMG) {
        this.BOOK_IMG = BOOK_IMG;
    }

    public String getBOOK_LINK() {
        return BOOK_LINK;
    }

    public void setBOOK_LINK(String BOOK_LINK) {
        this.BOOK_LINK = BOOK_LINK;
    }

    public String getBOOK_YEAR() {
        return BOOK_YEAR;
    }

    public void setBOOK_YEAR(String BOOK_YEAR) {
        this.BOOK_YEAR = BOOK_YEAR;
    }


}
