package ru.sviridoff.robots.robot_1;

import org.openqa.selenium.WebDriver;
import ru.sviridoff.framework.driver.SeleniumDriver;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;

import ru.sviridoff.robots.robot_1.libsteps.ApiSteps;
import ru.sviridoff.robots.robot_1.utils.Stash;
import ru.sviridoff.robots.robot_1.utils.Utils;
import sun.rmi.runtime.Log;

import java.io.IOException;

public class Robot_1 {

    private static Utils utils = new Utils();
    private static ApiSteps steps = new ApiSteps();
    private static Logger logger = new Logger();
    private static SeleniumDriver driver = new SeleniumDriver();

    public static void main(String[] args) throws IOException {


        // Upload Robot_1 to gitlab
        // Download from git to Win and run with VPN to check what link is correct or wrong.
        // Update books from telegram to Mail/Yandex cloud
        // Get from utils and driver packages to the Own FrameWork

        Stash.properties = new Props("props.properties");

        Stash.LogFile = "./log";
        logger.Logging(Stash.INFO, "Торжественно клянусь, что замышляю только шалость. (с)");
        logger.Logging(Stash.INFO, "Загрузчик на сайт - [STARTED].");

        steps.MainSteps();


        logger.Logging(Stash.SUCCESS, "Робот закончил свою работу.");
    }



}
